var gLogLevel = 4;

const Rollbar = require('rollbar');

const rollbar = Rollbar.init({
  accessToken: process.env.ROLLBAR_KEY,
  captureUncaught: false,
  captureUnhandledRejections: false 
});

var logLevels = {
		'NONE': -1,
		'LOG': 0,
		'ERROR': 1,
		'WARNING': 2,
		'INFO': 3,
		'DEBUG': 4,
	};

var logger = {
	'log': function(message)
	{
		if(gLogLevel >= logLevels.LOG) console.log(message);
	},
	'info': function(message)
	{
		if(gLogLevel >= logLevels.INFO) console.log(message);
	},
	'debug': function(message)
	{
		if(gLogLevel >= logLevels.DEBUG) console.log(message);
	},
	'error': function(message)
	{
		if(gLogLevel >= logLevels.ERROR) console.log(message);
	},
	'logLevel': logLevels,
	'setLogLevel': function(logLevel)
	{
		gLogLevel=logLevel;

	}
}

module.exports = logger;
