const sql = require('mssql');
const bodyParser = require('body-parser');


const logger = require('./logger');

logger.info('App starting up');

var express = require('express');
var app = express();

app.use(bodyParser.json());
require("./request_logger")(app);

app.set('port', (process.env.PORT || 5000));



app.use(express.static(__dirname + '/public'));

app.get('/hello',
	function(request, response) {
	    response.json({"response": `Hello World! Our sql address is ${process.env.SQL_SERVER_ADDRESS}`});
	}
       );

app.listen(app.get('port'),
	   function() {
		   logger.info("Node app is running at localhost: " + app.get('port'));
	   }
	  );

