var logger = require("./logger");

const util = require('util');

function log_request(req)
{
  logger.info("**********");
  logger.info("Request: " + JSON.stringify(req.path));
  logger.info("Query: " + JSON.stringify(req.query));
  logger.info("Body: " + JSON.stringify(req.body));
  logger.info("**********");
}

module.exports = function(app)
{
    app.use(
        function (req, res, next)
        {
            log_request(req);
            next();
        }
    );
}
